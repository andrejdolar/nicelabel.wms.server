﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WMS.Server.Entities
{
    public class UserProducts
    {
        [Key]
        public string UserId { get; set; }
                
        //[Required, MaxLength(256)]
        //public string UserName { get; set; }

        [Required]
        public int ProductQuantity { get; set; }

        [ForeignKey("UserId")]
        public IdentityUser User { get; set; }
    }
}
