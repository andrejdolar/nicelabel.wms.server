﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WMS.Server.Models
{
    public class UserProduct
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public int ProductQuantity { get; set; }
    }
}
