﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS.Server.Models
{
    public class UserProductViewModel
    {
        public int PageSize { get; set; }

        public int TotalCount
        {
            get { return this.UserProductList.Count(); }
        }

        public List<UserProduct> UserProductList { get; set; }
    }
}
