﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using WMS.Server.Entities;
using Microsoft.AspNetCore.Http;
using WMS.Server.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;

namespace WMS.Server.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("[controller]/[action]")]
    public class ProductController : Controller
    {
        private readonly ClaimsPrincipal _caller;
        private readonly UserDbContext _userDbContext;

        public ProductController(UserManager<IdentityUser> userManager, UserDbContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            _caller = httpContextAccessor.HttpContext.User;
            _userDbContext = dbContext;
        }

        // TODO Also implement authentication for Web UI, using different roles (admin, user), only admin has access to UserProductGrid       
        [AllowAnonymous]
        [HttpGet]
        public ViewResult Index()
        {          
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<PartialViewResult> UserProductGrid()
        {
            var ups = await _userDbContext.UserProducts.Include("User").Select(up => new UserProduct
            {
                Username = up.User.UserName,
                //Password = There shouldn't be ANY way to retrieve a password as plain text - NOT SECURE!!!
                ProductQuantity = up.ProductQuantity
            }).OrderByDescending(up => up.ProductQuantity).ToListAsync();

            return PartialView("_UserProductGrid", ups);
        }

        public async Task<bool> AddProductQuantity([FromBody] ProductDto product)
        {
            var userId = _caller.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

            var userProduct = await _userDbContext.UserProducts.SingleOrDefaultAsync(u => u.UserId == userId);

            if (userProduct == null)
            {
                await _userDbContext.AddAsync(new UserProducts()
                {
                    UserId = userId,
                    ProductQuantity = product.Quantity
                });
            }
            else
            {
                userProduct.ProductQuantity += product.Quantity;
            }

            try
            {
                var save = await _userDbContext.SaveChangesAsync();
                return (save > 0);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }


}